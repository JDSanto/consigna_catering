#language: es
@wip
Característica: Descuento los jueves
 
Antecedentes:
  Dado que mi cuit personal es "20445556667"
 
  Escenario: d1 - Casamiento pedido un jueves para dia habil
    Dado un evento "casamiento" con servicio "superior"
    Y se genera el día "2021-05-13"
    Y que está programado para el "2021-05-18" que es un día hábil
    Y que tendrá 300 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 60705.0
 
  Escenario: d2 - Casamiento pedido un jueves para dia no habil
    Dado un evento "casamiento" con servicio "normal"
    Y se genera el día "2021-05-20"
    Y que está programado para el "2021-05-23" que es un día no hábil
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 2351.25
 
  Escenario: d3 - Casamiento pedido un martes para dia no habil
    Dado un evento "casamiento" con servicio "normal"
    Y se genera el día "2020-12-29"
    Y que está programado para el "2021-05-17" que es un día no hábil
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 2475.0
 
  Escenario: d4 - Fiesta de 15 pedido un jueves
    Dado un evento "fiesta de 15" con servicio "normal"
    Y se genera el día "2021-04-22"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 1 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 332.5
 
  Escenario: d5 - Empresarial pedido un jueves
    Dado un evento "empresarial" con servicio "premium"
    Y se genera el día "2021-05-20"
    Y que está programado para el "2021-05-26" que es un día hábil
    Y que tendrá 30 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 7837.5
 
  Escenario: d6 - Empresarial pedido un sábado
    Dado un evento "empresarial" con servicio "normal"
    Y se genera el día "2021-12-11"
    Y que está programado para el "2021-12-15" que es un día hábil
    Y que tendrá 15 comensales con menú "carnie"
    Y que tendrá 15 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 7850.0